# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, Bool

TYPES = [
    ('char', 'Text'),
    ('char_translate', 'Text Translateable'),
    ('select', 'List'),
    ('binary', 'File'),
    ('number', 'Number')
]

STATES = {
    'readonly': Not(Bool(Eval('active'))),
}


class Attribute(ModelSQL, ModelView):
    'Attribute'
    _name = 'product.attribute'
    _description = __doc__

    name = fields.Char('Name', select=1, required=True, translate=True,
        states=STATES)
    active = fields.Boolean('Active')
    value_type = fields.Selection(TYPES, 'Type', required=True, select=1,
        states=STATES)
    description = fields.Text('Description', states=STATES)
    digits = fields.Integer('Digits', states={
        'invisible': Not(Equal(Eval('value_type'), 'number')),
        # TODO: Set field required after module upgrade to tryton 2.4
        #'required': Equal(Eval('value_type'), 'number'),
        'readonly': Not(Bool(Eval('active'))),
        })
    options = fields.One2Many('product.attribute.option', 'attribute',
        'Options', states={
            'invisible': Not(Equal(Eval('value_type'), 'select')),
            'required': Equal(Eval('value_type'), 'select'),
            'readonly': Not(Bool(Eval('active'))),
        }, depends=['active', 'value_type'])

    def __init__(self):
        super(Attribute, self).__init__()
        self._order.insert(0, ('name', 'ASC'))

    def default_active(self):
       return True

Attribute()


class AttributeOption(ModelSQL, ModelView):
    'Attribute Option'
    _name = 'product.attribute.option'
    _description = __doc__

    attribute = fields.Many2One('product.attribute', 'Attribute',
        required=True)
    name = fields.Char('Name', required=True, translate=True, loading='lazy')

AttributeOption()

