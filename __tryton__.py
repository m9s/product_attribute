#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Product Attribute',
    'name_de_DE': 'Artikel Attribut',
    'version': '2.2.0',
    'author': 'Open Labs Business Solutions, virtual things',
    'email': 'info@openlabs.co.in, info@virtual-things.biz',
    'website': 'http://www.openlabs.co.in/, http://www.virtual-things.biz',
    'description': '''
    - Provides extended attributes for products
    ''',
    'description_de_DE': '''
    - Stellt erweiterte Attribute für Artikel zur Verfügung
    ''',
     'depends': [
        'product',
    ],
    'xml': [
        'attribute.xml',
        'product.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
